# %%
print('Importing...')

import pandas as pd
import math
import matplotlib.pyplot as plt
import sys
import numpy as np

arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

#%%
def getIdent(config):
  print(config)
  if 'EX-FC-LS' in config:
    return 'Fast Convergence with Exploration & Local Search'
  elif 'PED-EX-FC-ME-RT' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation\nPedestrians\nReduced-Time'
  elif 'PED-EX-FC-ME' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation\nPedestrians'
  elif 'EX-FC-ME' in config:
    return 'Fast Convergence with Exploration\nRe-Evaluation'
  elif 'EX-FC' in config:
    return 'Fast Convergence with Exploration'
  elif 'PED-FC-ME-RT' in config:
    return 'Fast Convergence\nRe-Evaluation\nPedestrians\nReduced-Time'
  elif 'PED-FC-ME' in config:
    return 'Fast Convergence\nRe-Evaluation\nPedestrians'
  elif 'FC-ME' in config:
    return 'Fast Convergence\nRe-Evaluation'
  elif 'FC' in config:
    return 'Fast Convergence'
  elif 'PSOC' in config:
    return 'Patricle Swarm Optimisation (Calm)'
  elif 'PSOA' in config:
    return 'Patricle Swarm Optimisation (Active)'

#%%
df = pd.read_table(getArg(len(arguments) - 2, ''), sep=";")
fig = plt.figure(figsize=(20,15))
ax=fig.subplots();
ax.set_ylabel('Fitness')
ax.set_xlabel('Scaling')
positions = [df['Run'].min(), df['Run'].max()]
#ax.set_yticklabels(df['Fitness'])
#ax.set_xticklabels(df['Run'])
#ax.set_xticklabels([])
#ax.set_xticks([], [])
ax.set_xlim([-5, 106])
#print(df['Fitness'].max())
#ax.set_yticks(np.arange(df['Fitness'].min(), df['Fitness'].max(), step=10))

colors = ['#7b2cbf', '#00a6ed', '#7fb800', '#0d2c54', '#f6511d']
labels = ['Fixed-Plan', 'Adaptive-Plan', 'EA - FC-EX', 'EA - FC']
index = 0
for i in range(len(arguments) - 2, 3, -1):
  df = pd.read_table(getArg(i, ''), sep=";")
  ax.scatter(df['Run'], df['Fitness'], color=colors[index], s=15, alpha=0.5)

  z = np.polyfit(df['Run'], df['Fitness'], 1)
  p = np.poly1d(z)
  ax.plot(df['Run'],p(df['Run']),"r--", color=colors[index], label=labels[index])

  #ax.plot(df['Run'], df['Fitness'], '--', color=colors[index], alpha=0.2)
  index = index + 1

plt.legend(loc="upper left")

dfs = []
for i in range(3, 1, -1):
  config = getArg(i, '')
  print(config)
  print('Importing ' + config + ' values...')
  df = pd.read_table('./' + config + '/results-ea-batch-' + config + '-results.csv', sep=";")
  df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
  df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
  df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))
  df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]
  # avg. chromosomes
  print('Avg Chromosomes evaluated: %.2f' % df['Chromosomes'].mean())
  # avg. Fitness
  print('Avg Fitness: %.2f' % df_validFitness['Fitness'].mean())
  print('Max Fitness: %.2f' % df_validFitness['Fitness'].max())
  print('Min Fitness: %.2f' % df_validFitness['Fitness'].min())
  print('Avg. Generation: %.2f' % df_validFitness['GenerationFound'].mean())
  print('Chromosomes with valid fitness: ' + str(df_validFitness.shape[0]))
  print('Invalid Fitness: ' + str(df['Fitness'].shape[0] - df_validFitness.shape[0]))
  dfs.append(df_validFitness['Fitness'])

ax.boxplot(dfs, positions=positions, vert=True, showmeans=False, widths=1, boxprops= dict(color='#0d2c54'), whiskerprops=dict(color='#0d2c54'), capprops=dict(color='#0d2c54'), flierprops=dict(markeredgecolor='#0d2c54'))
ax.set_xticks(np.arange(1, 106, 5.0))

#plt.show()
plt.savefig(getArg(0, '') + '-robustness-new.pdf', bbox_inches='tight')


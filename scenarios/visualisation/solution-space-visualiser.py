# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
from colour import Color
import math

arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

addBatchResults = getArg(3, False)
createPlot = getArg(5, False)

# %%
print("Importing values...")
df = pd.read_table(getArg(0, 'test.csv'), sep=";")
df['Fitness'] = df['Fitness'].apply(lambda x: float(x))
df['Time-All'] = df['Time-All'].apply(lambda x: float(x))
df['Time-VIP'] = df['Time-VIP'].apply(lambda x: float(x))

uniques = len(df.drop_duplicates())
print("Duplicate values: " + str(df.shape[0] - uniques))
df = df.drop_duplicates()

if(getArg(1, False)):
  df_ea = pd.read_table(getArg(1, 'results-ea.csv'), sep=";")
  df_ea['Fitness'] = df_ea['Fitness'].apply(lambda x: float(x))
  df_ea['Time-All'] = df_ea['Time-All'].apply(lambda x: float(x))
  df_ea['Time-VIP'] = df_ea['Time-VIP'].apply(lambda x: float(x))
  print("Consistency checks...")
  if (df[df['Fitness'] > 0].shape[0] == getArg(2, 3096)):
    print('Solutions seem complete')
  else:
    print('Solutions are incomplete')

# %%
print("Creating computed values arrays...")

print('Creating color arrays...')
#colorsA = list(Color("yellow").range_to(Color("orange"),150))
colorsB = list(Color("yellow").range_to(Color("green"),50))
colors = colorsB

#colorsC = list(Color("lightblue").range_to(Color("blue"),50))
colorsD = list(Color("lightblue").range_to(Color("darkblue"),50))
colorsEA = colorsD

print('Calculating max fitness...')
minFitness = df.loc[df[df['Fitness'] > 0]['Fitness'].idxmin()][0]
if(getArg(1, False)):
  minFitness = min(minFitness, df_ea.loc[df_ea[df_ea['Fitness'] > 0]['Fitness'].idxmin()][0])
print('Min Fitness: ' + str(minFitness))
print('#-1: ' + str(df[df['Fitness'] == -1].shape[0]))
print('#-2: ' + str(df[df['Fitness'] == -2.0].shape[0]))
print('#>0: ' + str(df[df['Fitness'] > 0].shape[0]))
print('#>0<1.000.000: ' + str(df[df['Fitness'] > 0][df['Fitness'] < 1000000].shape[0]))

def calcColor(fitness, colArr):
  if fitness == -2:
    return (0.0, 0.0, 0.0, 0.07)
  elif fitness == -1:
    return (0.0, 0.0, 0.0, 0.80)
  else:
    fitnessPorportion = minFitness / fitness
    index = min(fitnessPorportion * len(colArr), len(colArr))
    rgbVal = colArr[int(round(index) - 1)].rgb
    #alpha = (minFitness + ((minFitness + fitness) * 3))/minFitness
    alpha = minFitness / (minFitness + ((minFitness + fitness) * 0.5))
    return (*rgbVal, max(alpha, 0.4))

def calcSize(fitness):
  if fitness == -2:
    return 20
  elif fitness == -1:
    return 1000
  else:
    fitnessPorportion = minFitness / fitness
    return 20 + (fitnessPorportion * 50000)

print('Assigning index & Updated...')
df['index'] = df.index
df['updated'] = False

if(createPlot):
  bound = int(getArg(6, 10))
  print('Creating colors...')
  df['Color'] = df['Fitness'].apply(lambda x: calcColor(x, colors))
  df['Edgecolor'] = df['Color']
  print('Creating sizes...')
  df['Size'] = df['Fitness'].apply(lambda x: calcSize(x))
  print('Assigning x vals...')
  df['x'] = df['index'].apply(lambda x: x % bound)
  print('Assigning y vals...')
  df['y'] = df['index'].apply(lambda x: math.ceil((x + 1) / bound))

print(df.head())
print(df.tail())

# %%
if(addBatchResults):
  print('Processing Batch results')
  df_batch = pd.read_table(getArg(4, 'results-ea-batch.csv'), sep=";")
  print('df_batch shape:' + str(df_batch.shape[0]) + 'x' + str(df_batch.shape[1]))
  avgGeneration = df_batch['GenerationFound'].mean()
  print(avgGeneration)


# %%

print('Creating BoxPlot...')
fig=plt.figure()

df_validFitness = df[df['Fitness'] > 0][df['Fitness'] < 1000000]

if(addBatchResults):
  fig = plt.figure(figsize=(10,2))
  ax=fig.add_axes([0,0,1,1])
  bp_dict = ax.boxplot([df_validFitness['Fitness'], df_batch['Fitness']], vert=False, showmeans=True)
  ax.set_xlabel('Fitness')
  ax.set_ylabel('')
  ax.set_yticklabels(['Brute-Force (' + str(int(round(df_validFitness.shape[0]))) + ')', 'EA Executions (avg. ' + str(int(round(df_batch['Chromosomes'].mean()))) + ')'])
else:
  fig = plt.figure(figsize=(10,1))
  ax=fig.add_axes([0,0,1,1])
  bp_dict = ax.boxplot(df_validFitness['Fitness'], vert=False, showmeans=True)
  ax.set_xlabel('Fitness')
  ax.set_ylabel('')
  ax.set_yticklabels(['Brute-Force (' + str(int(round(df_validFitness.shape[0]))) + ')'])

for index,line in enumerate(bp_dict['medians']):
  if(addBatchResults and index < 1) or not addBatchResults:
    x, y = line.get_xydata()[1]
    ax.annotate('%.1f' % x, xy=(x, y + 0.1))

for index,line in enumerate(bp_dict['means']):
  if(addBatchResults and index < 1) or not addBatchResults:
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x, y - .3))

for index,line in enumerate(bp_dict['caps']):
    movement = -0.06
    if(index % 2 > 0):
      movement = 0.04
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x + (x * movement), y - .3))

for index,line in enumerate(bp_dict['whiskers']):
  if(addBatchResults and index < 2) or not addBatchResults:
    x, y = line.get_xydata()[0]
    ax.annotate('%.1f' % x, xy=(x, y - .3))

plt.savefig('./search-space-2-4-5-30-box-plot.pdf', bbox_inches='tight')
plt.show()

# %%
if(createPlot and getArg(7, False)):
  print("adding EA Values...")
  matches = 0
  for index,row in df.loc[(df['Durations'].isin(df_ea['Durations'])) & (df['Signals'].isin(df_ea['Signals']))].iterrows():
    for i, eaEntry in df_ea.iterrows():
      if eaEntry['Durations'] == row['Durations'] and eaEntry['Signals'] == row['Signals']:
        df.at[index,'Fitness'] = eaEntry['Fitness']
        df.at[index,'Edgecolor'] = row['Color']
        df.at[index,'Color'] = calcColor(eaEntry['Fitness'], colorsEA)
        df.at[index,'Size'] = calcSize(eaEntry['Fitness'])
        df.at[index,'updated'] = True
        matches = matches + 1
        break
  print('Matches: ' + str(matches))
  print(df[df['updated'] == True])
  #print("Not matched:")
  #print(df.loc[(~df_ea['Durations'].isin(df['Durations'])) & (~df_ea['Signals'].isin(df['Signals']))])

#%%
if(createPlot):
  print("Creating plot...")
  fig=plt.figure()
  fig = plt.figure(figsize=(300,300))
  ax=fig.add_axes([0,0,1,1])
  ax.scatter(df['x'], df['y'], c = df['Color'], edgecolors=df['Edgecolor'], s = df['Size'])
  ax.set_xticklabels([])
  ax.set_yticklabels([])
  plt.savefig('./search-space-2-4-5-30.png', bbox_inches='tight')
  print('Saved png')
  #plt.savefig('./search-space-2-4-5-30.pdf', bbox_inches='tight')
  plt.show()

# %%

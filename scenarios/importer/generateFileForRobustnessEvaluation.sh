#!/bin/zsh
iterations=100

northLowerBound=24
northUpperBound=100
northStepSize=$(awk "BEGIN {print ($northUpperBound-$northLowerBound)/$iterations}")

eastLowerBound=4
eastUpperBound=6
eastStepSize=$(awk "BEGIN {print ($eastUpperBound-$eastLowerBound)/$iterations}")

southLowerBound=8
southUpperBound=50
southStepSize=$(awk "BEGIN {print ($southUpperBound-$southLowerBound)/$iterations}")

westLowerBound=4
westUpperBound=6
westStepSize=$(awk "BEGIN {print ($westUpperBound-$westLowerBound)/$iterations}")

echo $northStepSize $eastStepSize $southStepSize $westStepSize

for i in `seq 0 $iterations`;
  do
    north=$(awk "BEGIN {print $northLowerBound+($i*$northStepSize)}")
    east=$(awk "BEGIN {print $eastLowerBound+($i*$eastStepSize)}")
    south=$(awk "BEGIN {print $southLowerBound+($i*$southStepSize)}")
    west=$(awk "BEGIN {print $westLowerBound+($i*$westStepSize)}")
    echo $north $east $south $west
    python3 ./realTimeTrafficDataImporter-Flows.py ../596-finalised/trafficData.csv ../596-finalised/layout.json $north $east $south $west ./tmp/base-$i.rou.xml
  done
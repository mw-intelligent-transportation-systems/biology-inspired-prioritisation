'''
 -> Script to convert real time traffic data to SUMO route file
 -> The script produces flows (neglects pedestrian)
'''

# %%
import numpy as np
import pandas as pd
import sys
import json
import math

# %%
arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

# %%
real_time_traffic_data = pd.read_csv(getArg(0, '../596-morning/trafficData.csv'), sep=';')
with open(getArg(1, '../596-morning/layout.json')) as json_file:
    layout = json.load(json_file)

# %%
header = '''\
<?xml version="1.0" encoding="UTF-8"?>

<routes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/routes_file.xsd">
  '''

vehicle_types = '''\
    <!-- Vehicle Types -->

    <vType id="bus" vClass="bus"/>
    <vType id="taxi" vClass="taxi"/>
    <vType id="passenger" vClass="passenger"/>
    <vType id="truck" vClass="truck"/>
    <vType id="motorcycle" vClass="motorcycle"/>
    <vType id="bicycle" vClass="bicycle"/>

    <vType id="pedestrian" vClass="pedestrian"/>

    <!-- Vehicle Types -->
    '''

content = ''

footer = '''\
</routes>
  '''

#%%
flows = []

for index,row in real_time_traffic_data.iterrows():

  if(row['Type'] == 'pedestrian'):
    continue

  if(row['VehPerHour'] == 0):
    continue

  laneCount = 2
  if(row['From'] == 'North' or row['From'] == 'South'):
    laneCount = 1

  for i in range(0, laneCount):
    # 6 time intervalls -> 0, 20, 40, 60, 80, 100, 120
    timeInterval = 20
    timeIntervalls = math.ceil(120 / timeInterval)
    for n in range(0, timeIntervalls):

      if(n % 2 > 0):
        continue

      flowId = str(index) + "-" + str(i) + "-" + str(n)
      departSpeed = 'max'
      color = 'yellow'
      if(row['Type'] == 'pedestrian'):
        color = 'blue'
      vehicleType = row['Type']
      begin = n * timeInterval
      end = begin + timeInterval
      departLane = str(i)

      edgeA = 1
      factor = float(getArg(2, 1))
      if(row['From'] == 'East'):
        edgeA = 3
        factor = float(getArg(3, 1))
      elif(row['From'] == 'South'):
        edgeA = 5
        factor = float(getArg(4, 1))
      elif(row['From'] == 'West'):
        edgeA = 7
        factor = float(getArg(5, 1))

      edgeB = 2
      if(row['To'] == 'East'):
        edgeB = 4
      elif(row['To'] == 'South'):
        edgeB = 6
      elif(row['To'] == 'West'):
        edgeB = 8

      edges = str(edgeA) + " " + str(edgeB)

      vehPerHour = row['VehPerHour']
      vehPerSecond = vehPerHour * factor / 60 / 60
      laneFactor = (laneCount - 0)
      if(row['Type'] == 'pedestrian'):
        laneFactor = 1
      vehPerSecondPerLane = vehPerSecond / laneFactor
      probability = vehPerSecondPerLane

      if(row['Type'] == 'pedestrian'):
        flows.append(
          [
            begin,
            '<personFlow id="' + flowId + '" color="' + color + '" begin="' + str(begin) + '" end="' + str(end) + '" probability="' + str(probability) + '" type="' + vehicleType + '"><walk edges="' + edges + '"/></personFlow>\n'
          ]
        )
      else:
        flows.append(
          [
            begin,
            '<flow id="' + flowId + '" color="' + color + '" begin="' + str(begin) + '" end="' + str(end) + '" probability="' + str(probability) + '" departLane="' + departLane + '" type="' + vehicleType + '" departSpeed="' + departSpeed + '"><route edges="' + edges + '"/></flow>\n'
          ]
        )

flows.append(
  [
    20,
    '<vehicle id="VIP" depart="20" departPos="0" departSpeed="max" departLane="0" type="bus"><route edges="5 8" color="red"/></vehicle>'
  ]
)
# %%
content = ""

for flow in sorted(flows):
  content = content + flow[1]

# %%
f = open(getArg(6, '../596-morning/flows.rou.xml'), "w")
f.write(header + vehicle_types + content + footer)
f.close()

# %%

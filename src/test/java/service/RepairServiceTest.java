package service;

import common.service.RepairService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class RepairServiceTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("UNIT-TESTS");
    }

    @Test
    void repairTooLongDuration() {
        List<Integer> repairedDurations = RepairService.repairDurations(Arrays.asList(1, 2, 3, 4, 5));
        Assertions.assertEquals(repairedDurations, Arrays.asList(1, 2, 3, 4));
    }

    @Test
    void repairValidDuration() {
        List<Integer> repairedDurations = RepairService.repairDurations(Arrays.asList(1, 2, 3, 4));
        Assertions.assertEquals(repairedDurations, Arrays.asList(1, 2, 3, 4));
    }

    @Test
    void repairTooLongSignals() {
        List<Integer> repairedSignals = RepairService.repairDurations(Arrays.asList(1, 2, 3, 4, 5));
        Assertions.assertEquals(repairedSignals, Arrays.asList(1, 2, 3, 4));
    }

    @Test
    void repairValidSignals() {
        List<Integer> repairedSignals = RepairService.repairDurations(Arrays.asList(1, 2, 3, 4));
        Assertions.assertEquals(repairedSignals, Arrays.asList(1, 2, 3, 4));
    }

    @Test
    void repairConsecutiveSignalsA() {
        List<Integer> repairedSignals = RepairService.repairSignals(Arrays.asList(2, 3, 4, 4));
        Assertions.assertEquals(repairedSignals, Arrays.asList(2, 3, 2, 3));
    }

    @Test
    void repairConsecutiveSignalsB() {
        List<Integer> repairedSignals = RepairService.repairSignals(Arrays.asList(1, 2, 2, 3));
        Assertions.assertEquals(repairedSignals, Arrays.asList(1, 2, 3, 2));
    }

}
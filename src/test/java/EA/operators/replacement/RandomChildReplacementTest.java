package EA.operators.replacement;

import DTO.Fitness;
import common.data.Chromosome;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;

class RandomChildReplacementTest {

    RandomChildReplacement randomChildReplacement;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        randomChildReplacement = new RandomChildReplacement();
    }

    @Test
    void replace() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 10);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 10);
        Chromosome chromosomeC = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 11);

        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB, chromosomeC));
        Chromosome child = new Chromosome("D", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 12);
        ConcurrentLinkedQueue<Chromosome> newChromosomes = randomChildReplacement.replace(chromosomes, child);
        Assertions.assertEquals(newChromosomes.size(), 3);
        Assertions.assertTrue(newChromosomes.contains(child));
    }
}
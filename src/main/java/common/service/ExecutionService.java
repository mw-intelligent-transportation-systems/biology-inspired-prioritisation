package common.service;

import EA.EAService;
import common.data.Chromosome;
import PSO.PSOService;
import configuration.Configuration;
import localSearch.HillClimber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import transformers.ChromosomeTransformer;

import java.util.concurrent.*;

// This service is responsible for executing a single EA or PSO
public class ExecutionService {

    private static final Logger logger = LogManager.getLogger(ExecutionService.class.getName());

    /**
     * Sets the configuration
     * @param config the config name as String
     */
    public static void setConfiguration(String config){
        new Configuration(config);
    }

    /**
     * Runs the Evolutionary Algorithm (EA) and outputs results
     * Additionally, the best solution is exported to ./scenarios/signals-best.sig.xml
     * First the EA is run and then a local search is executed -> based on the configured time limits
     */
    public static void runEA(long start, String additionalConfig){
        final ExecutorService service = Executors.newSingleThreadExecutor();

        logger.info("Starting Application...");
        EAService.additionalConfig = additionalConfig;
        Future future = service.submit(EAService::runEA);

        try {
            future.get(Configuration.rc.timeLimit() - Configuration.rc.timeLimitLocalSearch(), TimeUnit.MILLISECONDS);
        } catch (final TimeoutException | InterruptedException | ExecutionException e) {
            // Terminate EA when time limit is reached
            logger.info("Stopped EA after " + ((System.nanoTime() - start) / 1000000) + " ms.");
            e.printStackTrace();
        } finally {
            // Shutdown service
            service.shutdownNow();
            ParallelisationService.abort();

            Double bestBeforeLocalSearch = StatisticsService.result.getBestOverall().getFitness().getValueRegardlessOfSharedFitness();

            // Start Local Search
            final ExecutorService localSearchService = Executors.newSingleThreadExecutor();
            Future localSearchFuture = localSearchService.submit(HillClimber::execute);

            try {
                localSearchFuture.get(Configuration.rc.timeLimitLocalSearch(), TimeUnit.MILLISECONDS);
            } catch (final TimeoutException | InterruptedException | ExecutionException e) {
                logger.info("Stopped Local Search.");
                e.printStackTrace();
            } finally {
                // Shutdown service
                localSearchService.shutdownNow();
                ParallelisationService.abort();
                // Export statistics
                Double bestAfterLocalSearch = StatisticsService.result.getBestOverall().getFitness().getValueRegardlessOfSharedFitness();
                logger.info("Best before LS: " + bestBeforeLocalSearch + " best after LS: " + bestAfterLocalSearch);
                StatisticsService.addRemainingForAvgFitness();
                StatisticsService.addRemainingForAvgDistance();
                terminate((double) (System.nanoTime() - start) / 1000000);
            }
        }
    }

    /**
     * Runs a PSO
     * @param start the timestamp when the algorithm was started
     */
    public static void runPSO(long start){
        final ExecutorService service = Executors.newSingleThreadExecutor();
        logger.info("Starting Application...");
        Future future = service.submit(PSOService::runPSO);
        try {
            future.get(Configuration.rc.timeLimit(), TimeUnit.MILLISECONDS);
        } catch (final TimeoutException | InterruptedException | ExecutionException e) {
            logger.info("Stopped PSO after " + ((System.nanoTime() - start) / 1000000) + " ms.");
            e.printStackTrace();
        } finally {
            service.shutdownNow();
            ParallelisationService.abort();
            CleanUpService.cleanUp();
            terminate((double) (System.nanoTime() - start) / 1000000);
        }
    }

    /**
     * Terminates the application
     * The best solutions are exported
     * Various statistics are displayed
     * Stats also written to a specified file for batch statistics
     * @param time the time the algorithm ran
     */
    private static void terminate(Double time){
        ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestOverall(), "best-overall");
        ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestForTargetVehicle(), "best-target-vehicle");
        ChromosomeTransformer.transformChromosome(StatisticsService.result.getBestForAllVehicles(), "best-all-vehicles");
        logger.info("Best solutions exported");
        CleanUpService.cleanUp();
        logger.info("Clean up completed\n");

        try{
            logger.info("Statistics:");
            logger.info("\t\tDefault Signal Plan with fitness: " + StatisticsService.result.getFitnessOfDefaultSignals().getValue() + " - time for target vehicle: " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForAllVehicles());
            logger.info("\t\tBest Overall result with fitness: " + StatisticsService.result.getBestOverall().getFitness().getValue() + "(" + StatisticsService.result.getBestOverall().getGenerationBorn()  + ")" +  " - time for target vehicle: " + StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles());
            logger.info("\t\tBest Target Vehicle result with fitness: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getValue() + "(" + StatisticsService.result.getBestForTargetVehicle().getGenerationBorn()  + ")" + " - time for target vehicle: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForTargetVehicle().getFitness().getTimeForAllVehicles());
            logger.info("\t\tBest All Vehicles result with fitness: " + StatisticsService.result.getBestForAllVehicles().getFitness().getValue() + "(" + StatisticsService.result.getBestForAllVehicles().getGenerationBorn()  + ")" + " - time for target vehicle: " + StatisticsService.result.getBestForAllVehicles().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForAllVehicles().getFitness().getTimeForAllVehicles() + "\n");

            logger.info("\t\tBest Overall result in initial Population with fitness: " + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForAllVehicles());
            logger.info("\t\tBest Target Vehicle result in initial Population with fitness: " + StatisticsService.result.getBestForTargetVehicleInInitialPopulation().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestForTargetVehicleInInitialPopulation().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForTargetVehicleInInitialPopulation().getFitness().getTimeForAllVehicles());
            logger.info("\t\tBest All Vehicles result in initial Population with fitness: " + StatisticsService.result.getBestForAllVehiclesInInitialPopulation().getFitness().getValue() + " - time for target vehicle: " + StatisticsService.result.getBestForAllVehiclesInInitialPopulation().getFitness().getTimeForTargetVehicle() + " - time for all vehicles: " + StatisticsService.result.getBestForAllVehiclesInInitialPopulation().getFitness().getTimeForAllVehicles() + "\n");

            logger.info("Metrics:");
            logger.info("\t\tGeneration: " + Configuration.ac.generations());
            logger.info("\t\tEvaluated: " + StatisticsService.result.getEvaluatedChromosomes());
            if(Configuration.ac.numberOfChildren() != null)
                logger.info("\t\tChromosomes: " + (Configuration.ac.populationSize() + (Configuration.ac.generations() *Configuration.ac.numberOfChildren())) + " (" + Configuration.ac.populationSize() + ")\n");

            logger.info("Default Signal Plan:");
            logger.info("\t\t" + StatisticsService.result.getBestOverall().getFitness().getValue() + " - " + StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle() + " - " + StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles());
            logger.info("\t\t" + StatisticsService.result.getFitnessOfDefaultSignals().getValue() + " - " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForTargetVehicle() + " - " + StatisticsService.result.getFitnessOfDefaultSignals().getTimeForAllVehicles());
            logger.info("\t\t" + String.format("%.2f", StatisticsService.result.getFitnessOfDefaultSignals().getValue()/ StatisticsService.result.getBestOverall().getFitness().getValue()) + " - " + String.format("%.2f", StatisticsService.result.getFitnessOfDefaultSignals().getTimeForTargetVehicle()/ StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle()) + " - " + String.format("%.2f", StatisticsService.result.getFitnessOfDefaultSignals().getTimeForAllVehicles()/ StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles()) + "\n");

            logger.info("Initial Population:");
            logger.info("\t\t" + StatisticsService.result.getBestOverall().getFitness().getValue() + " - " + StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle() + " - " + StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles());
            logger.info("\t\t" + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getValue() + " - " + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForTargetVehicle() + " - " + StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForAllVehicles());
            logger.info("\t\t" + String.format("%.2f", StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getValue()/ StatisticsService.result.getBestOverall().getFitness().getValue()) + " - " + String.format("%.2f", StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForTargetVehicle()/ StatisticsService.result.getBestOverall().getFitness().getTimeForTargetVehicle()) + " - " + String.format("%.2f", StatisticsService.result.getBestOverallInInitialPopulation().getFitness().getTimeForAllVehicles()/ StatisticsService.result.getBestOverall().getFitness().getTimeForAllVehicles()) + "\n");
            logger.info("Finished Running Application (" + String.format("%.2f", time) + " ms)");
        }catch (Exception e){
            logger.error("Could not output statistics.");
        }

        writeSolutions();
        writeDistances();
    }

    /**
     * The best solutions is written to a specified file
     */
    private static void writeSolutions(){
        String fileName = "results-ea-" + Configuration.getIdentifier() + ".csv";
        StatisticsService.deleteFile(fileName);
        StatisticsService.writeTxt(fileName, "Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        for(Chromosome chromosome : StatisticsService.result.getEvaluatedChromosomeList()){
            String str = chromosome.getFitness().getValueRegardlessOfSharedFitness() + ";" + chromosome.getFitness().getTimeForTargetVehicle() + ";" + chromosome.getFitness().getTimeForAllVehicles() + ";" + chromosome.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + BruteForceService.getSignalIndex(chromosome) + ";" + chromosome.getDurations().toString() + ";" + chromosome.getSignals().toString() + "\n";
            StatisticsService.writeTxt(fileName, str);
        }
    }

    /**
     * Information about diversity are written to a specified file
     */
    private static void writeDistances(){
        String fileName = "results-ea-distances-" + Configuration.getIdentifier() + ".csv";
        StatisticsService.deleteFile(fileName);
        for(Chromosome chromosome : StatisticsService.result.getEvaluatedChromosomeList()){
            String str = chromosome.getFitness().getValueRegardlessOfSharedFitness().toString();
            for(Chromosome c : StatisticsService.result.getEvaluatedChromosomeList()){
                str = str  + ";" + ChromosomeComparisonService.calculateEuclideanDistance(chromosome, c).toString();
            }
            StatisticsService.writeTxt(fileName, str + "\n");
        }
    }

}

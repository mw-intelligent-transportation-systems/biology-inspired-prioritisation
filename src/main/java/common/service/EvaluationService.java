package common.service;

import DTO.Fitness;
import DTO.Penalty;
import DTO.SimulationResults;
import configuration.Configuration;
import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

// Service with only static methods for evaluating a single Chromosome using SUMO
public class EvaluationService {

    private static final Logger logger = LogManager.getLogger(EvaluationService.class.getName());

    /**
     * Evaluation of a single Chromosome
     * Evaluation is done three times to ensure solution robustness
     * @param signals the file name of the XML containing the signals to evaluate
     * @param id the id of the chromosome, used to identify the XML
     * @return the Fitness of the chromosome
     */
    public static Fitness evaluate(String signals, String id){
        if(Configuration.ac.executeMultipleEvaluations()){
            Fitness defaultFitness = evaluateSingle(signals, id, false);
            Fitness randomFitnessA = evaluateSingle(signals, id, true);
            Fitness randomFitnessB = evaluateSingle(signals, id, true);
            Double fitnessValue = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getValue).average().orElse(Double.MAX_VALUE);
            Double fitnessVIP = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getTimeForTargetVehicle).average().orElse(Double.MAX_VALUE);
            Double fitnessAll = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getTimeForAllVehicles).average().orElse(Double.MAX_VALUE);
            return new Fitness(fitnessValue, fitnessVIP, fitnessAll, defaultFitness.getTotalPenalty(), defaultFitness.getWasAborted());
        }
        return evaluateSingle(signals, id, false);
    }

    /**
     * Evaluation of a single Chromosome
     * @param signals the file name of the XML containing the signals to evaluate
     * @param id the id of the chromosome, used to identify the XML
     * @return the Fitness of the chromosome
     */
    private static Fitness evaluateSingle(String signals, String id, boolean random){
        SimulationResults simulationResults = runSimulationManually(signals, id, random);
        if(StatisticsService.result != null)
            StatisticsService.result.addEvaluated();
        return calculateFitness(simulationResults);
    }

    /**
     * Evaluation of the default plan fixed/adaptive
     * @return the Fitness of the default plan
     */
    public static Fitness evaluateDefault(){
        String signals = Paths.get(".").toAbsolutePath().normalize().toString() + "/scenarios/" + Configuration.sc.defaultSignals();

        if(Configuration.ac.executeMultipleEvaluations()){
            Fitness defaultFitness = evaluateSingle(signals, "default", false);
            Fitness randomFitnessA = evaluateSingle(signals, "default", true);
            Fitness randomFitnessB = evaluateSingle(signals, "default", true);
            Double fitnessValue = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getValue).average().orElse(Double.MAX_VALUE);
            Double fitnessVIP = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getTimeForTargetVehicle).average().orElse(Double.MAX_VALUE);
            Double fitnessAll = Stream.of(defaultFitness, randomFitnessA, randomFitnessB).mapToDouble(Fitness::getTimeForAllVehicles).average().orElse(Double.MAX_VALUE);
            return new Fitness(fitnessValue, fitnessVIP, fitnessAll, defaultFitness.getTotalPenalty(), defaultFitness.getWasAborted());
        }
        return evaluateSingle(signals, "default", false);
    }

    /**
     * Fitness function
     * -> Only public for tests
     * -> The importance of the bus and the overall traffic can be configured
     * -> Additionally, penalties are considered, worsening the fitness
     */
    public static Fitness calculateFitness(SimulationResults simulationResults){
        Double fitness = (simulationResults.getTimeForTargetVehicle() * Configuration.rc.importanceOfTargetVehicle()) + (simulationResults.getTimeForAllVehicles() * (1 - Configuration.rc.importanceOfTargetVehicle()));
        Double totalPenalty = 0.0;
        for (Penalty penalty : simulationResults.getPenalties()) {
            switch (penalty.getReason()) {
                case JAM:
                    totalPenalty += fitness * 0.01;
                    break;
                case COLLISION:
                    totalPenalty += fitness * 1.0;
                    break;
                case EMERGENCY_BREAK:
                    totalPenalty += fitness * 0.01;
                    break;
                case EMERGENCY_STOP:
                    totalPenalty += fitness * 0.01;
                    break;
            }
        }
        fitness = fitness + totalPenalty;
        return new Fitness(fitness, simulationResults.getTimeForTargetVehicle(), simulationResults.getTimeForAllVehicles(), totalPenalty, simulationResults.getWasAborted());
    }

    /**
     * Runs the SUMO simulation manually by executing shell commands
     * @param signals the file name of the XML containing the signals to evaluate
     * @param id the id of the chromosome, used to identify the XML
     * @return the results of the simulation
     */
    private static SimulationResults runSimulationManually(String signals, String id, boolean random) {

        String parent = Paths.get(".").toAbsolutePath().normalize().toString();
        SimulationResults simulationResults = new SimulationResults();

        ProcessBuilder builder = new ProcessBuilder();
        /**
         * the sumo command takes in
         *  -> -n -> the network (static for all simulation)
         *  -> -r -> the routes / trips containing the cars (static for all simulations)
         *  -> -a -> the signals as additional file (individual per simulation)
         *  -> --end -> the time when the simulation should end, set by scenario configuration
         *  -> --collision.mingap-factor 0 -> limit collisions to physical
         *  -> --collision.check-junctions -> to check collisions between vehicles on the same intersection
         *  -> --collision.action none -> do nothing when collision occurs
         *  -> --time-to-teleport -1 -> disable teleportation
         *  -> --output-prefix -> the prefix used for all output files, the id of this simulation
         *  -> --tripinfo-output -> where to store the tripinfo output
         *  -> --tripinfo-output.write-unfinished -> write unfinished vehicles to XML when simulation is aborted (end time reached) in order to make XML valid
         *  -> --error-log -> where to store error logs
         *  --> --random -> give some random behaviour
         * the sumo simulation outputs a tripinfo xml which is identified by the chromosome id
         */
        String randomStr = random ? " --random" : "";
        builder.command("sh", "-c", "" + parent + "/sumo/bin/sumo -n " + parent + "/scenarios/" + Configuration.sc.network() + " -r " + parent + "/scenarios/" + Configuration.sc.routes() + " -a " + signals + " --end " + Configuration.sc.maxSimulationTime() + " --collision.mingap-factor 0 --collision.check-junctions --collision.action none --time-to-teleport -1" + randomStr + " --output-prefix " + id + " --tripinfo-output " + parent + "/scenarios/tmp/trips/-trips.out.xml --tripinfo-output.write-unfinished --error-log " + parent + "/scenarios/tmp/errors/-errors.out.txt");

        try {
            // run the simulation
            Process process = builder.start();
            StreamGobblerService streamGobblerService = new StreamGobblerService(process.getInputStream(), null);
            Executors.newSingleThreadExecutor().submit(streamGobblerService);
            int exitCode = process.waitFor();
            assert exitCode == 0;

            simulationResults = calculateJourneyTimes(parent, id);
            simulationResults.setPenalties(calculatePenalties(parent, id));

            process.getOutputStream().close();
            process.getErrorStream().close();
            process.getInputStream().close();
            process.destroy();

        } catch (IOException | InterruptedException e) {
            logger.warn("Simulation aborted");
        }

        return simulationResults;
    }

    /**
     * Extract the journey times for the bus and all vehicles
     * -> Only public for tests
     * @param parent current path in the system
     * @param id the id of the chromosome
     * @return the SimulationResults
     */
    public static SimulationResults calculateJourneyTimes(String parent, String id){
        Double timeForTargetVehicle = null;
        Double timeForAllRoadUsers = null;
        Boolean wasAborted = false;
        try{
            // get result XML
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new File(parent + "/scenarios/tmp/trips/" + id + "-trips.out.xml"));
            NodeList nodeListTripInfos = document.getElementsByTagName("tripinfo");
            NodeList nodeListPersonInfos = document.getElementsByTagName("walk");

            /**
             * Extract the arrival time of the target vehicle and the total time of the simulation from the xml
             * TripInfos are sorted by arrival time descending
             */
            for(int i = 0, size = nodeListTripInfos.getLength(); i < size; i++) {
                if(nodeListTripInfos.item(i).getAttributes().getNamedItem("id").getNodeValue().equals(Configuration.sc.idOfTargetVehicle())){
                    Double arrivalTime = Double.valueOf(nodeListTripInfos.item(i).getAttributes().getNamedItem("arrival").getNodeValue());
                    if(arrivalTime.equals(-1.0)){
                        wasAborted = true;
                        arrivalTime = Double.MAX_VALUE;
                    }
                    timeForTargetVehicle = arrivalTime;
                    if(i == nodeListTripInfos.getLength() - 1){
                        timeForAllRoadUsers = arrivalTime;
                    }
                }else if(i == nodeListTripInfos.getLength() - 1){
                    Double arrivalTime = Double.valueOf(nodeListTripInfos.item(i).getAttributes().getNamedItem("arrival").getNodeValue());
                    if(arrivalTime.equals(-1.0)){
                        wasAborted = true;
                        arrivalTime = Double.MAX_VALUE;
                    }
                    timeForAllRoadUsers = arrivalTime;
                }
            }

            /**
             * Extract the arrival time of persons from the xml
             * TripInfos are sorted by arrival time descending
             */
            if(nodeListPersonInfos.getLength() > 0){
                Double arrivalOfLastPerson = Double.valueOf(nodeListPersonInfos.item(nodeListPersonInfos.getLength() - 1).getAttributes().getNamedItem("arrival").getNodeValue());
                arrivalOfLastPerson = arrivalOfLastPerson.equals(-1.0) ? Double.MAX_VALUE : arrivalOfLastPerson;
                timeForAllRoadUsers = timeForAllRoadUsers < arrivalOfLastPerson ? arrivalOfLastPerson : timeForAllRoadUsers;
            }
        }catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Could not extract trip information");
        }

        return new SimulationResults(timeForTargetVehicle, timeForAllRoadUsers, new ArrayList<>(), wasAborted);
    }

    /**
     * Penalties are calculated by iterating through the generated error log
     * @param parent current path in the system
     * @param id the id of the chromosome
     * @return the List of penalties
     */
    public static List<Penalty> calculatePenalties(String parent, String id){
        List<Penalty> penalties = new ArrayList<>();
        Path path = Paths.get(parent + "/scenarios/tmp/errors/" + id + "-errors.out.txt");
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEachOrdered(line-> {
                if (line.contains("jammed")) {
                    penalties.add(new Penalty(Penalty.Reason.JAM));
                }else if (line.contains("Collision")){
                    penalties.add(new Penalty(Penalty.Reason.COLLISION));
                }else if (line.contains("emergency breaking")){
                    penalties.add(new Penalty(Penalty.Reason.EMERGENCY_BREAK));
                }else if (line.contains("emergency stop")){
                    penalties.add(new Penalty(Penalty.Reason.EMERGENCY_STOP));
                }
            });
        } catch (IOException e) {
            logger.error("Could not read error txt file for: " + id);
        }
        return penalties;
    }

}

package common.service;

import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.*;

// Service with only static methods used for multi threading
public class ParallelisationService {

    private static final Logger logger = LogManager.getLogger(ParallelisationService.class.getName());

    // Every chromosome needs have a unique id, these are taken from this thread safe list
    public static ConcurrentLinkedQueue<String> uids = new ConcurrentLinkedQueue<String>();

    private static ExecutorService executorService;

    /**
     * Generation of ids for chromosomes
     * To avoid concurrency errors a LinkedBlockingQueue is used (thread safe)
     * @param n the number of ids to create
     */
    public static void generateIds(int n){
        for(int i = 0; i < n; i++){
            uids.add(UUID.randomUUID().toString());
        }
        // Ensure uniqueness
        if(Arrays.stream(ParallelisationService.uids.toArray()).distinct().toArray(String[]::new).length != n){
            logger.error("Number of unique ids is not equal to number of ids.");
        }
        logger.info("Generated " + uids.size() + " unique Ids");
    }

    /**
     * Create n threads to be used in parallel
     * @param numberOfThreads the number of threads
     */
    public static void prepareExecution(int numberOfThreads){
        int threads = Configuration.ac.multiThreading() ? numberOfThreads : 1;
        executorService = Executors.newFixedThreadPool(threads);
        logger.debug("Number of threads: " + threads);
    }

    /**
     * Execute the task in parallel
     * @param task the task to run
     */
    public static void execute(Runnable task){
        executorService.execute(task);
    }

    // Wait until all tasks have finished running
    public static void finishExecution(){
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            logger.warn("Parallelisation task aborted");
        } finally {
            executorService.shutdownNow();
            executorService = null;
        }
    }

    // Forcefully aborts all executions
    static void abort(){
        if(executorService != null){
            executorService.shutdownNow();
            executorService = null;
        }
        uids = new ConcurrentLinkedQueue<String>();
    }

}

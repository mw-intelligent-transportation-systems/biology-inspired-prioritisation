package common.service;

import common.data.Chromosome;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The batch run service can be used to run a given configuration n times
 * The results are written to three files
 *  -> The results (e.g. best fitness per run)
 *  -> The evolution of the best fitness
 *  -> The evolution of the mean distance of all chromosomes
 * See readme in root folder for more details
 */
public class BatchRunService {


    private static final Logger logger = LogManager.getLogger(BatchRunService.class.getName());

    // The file names for the results, dependent on the configuration name
    private static String resultsFileName = "results-ea-batch-" + Configuration.getIdentifier() + "-results.csv";
    private static String evolutionFileName = "results-ea-batch-" + Configuration.getIdentifier() + "-evolution.csv";
    private static String distancesFileName = "results-ea-batch-" + Configuration.getIdentifier() + "-distances.csv";

    // Executes an EA with a given configuration n times
    public static void prepareAndExecuteEA(int numberOfRuns, String defaultConfig, String switchConfig){
        StatisticsService.deleteFile(resultsFileName);
        StatisticsService.writeTxt(resultsFileName, "Run;Chromosomes;GenerationFound;Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        StatisticsService.currentGeneration = 0;
        for(int i = 1; i <= numberOfRuns; i++){
            logger.info("Running EA: " + i + "/" + numberOfRuns);
            long start = System.nanoTime();
            ExecutionService.runEA(start, switchConfig);
            writeBest(i, StatisticsService.result.getBestOverall());
            Configuration.setConfig(defaultConfig);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        writeEvolutionAndDistance();
    }

    // Executes a PSO n times
    public static void prepareAndExecutePSO(int numberOfRuns){
        StatisticsService.deleteFile(resultsFileName);
        StatisticsService.writeTxt(resultsFileName, "Run;Chromosomes;GenerationFound;Fitness;Time-VIP;Time-All;Duration-Sum;Signal-Index;Durations;Signals\n");
        StatisticsService.currentGeneration = 0;
        for(int i = 1; i <= numberOfRuns; i++){
            logger.info("Running PSO: " + i + "/" + numberOfRuns);
            long start = System.nanoTime();
            ExecutionService.runPSO(start);
            writeBest(i, StatisticsService.result.getBestOverall());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        writeEvolutionAndDistance();
    }

    // writes best solution of a single EA/PSO to a predefined file
    private static void writeBest(int run, Chromosome best){
        String str = run + ";-1;-1;-1;-1;-1;-1;-1;-1;-1\n";
        try{
            str = run + ";" + StatisticsService.result.getEvaluatedChromosomes() + ";" + best.getGenerationBorn() + ";" + best.getFitness().getValue() + ";" + best.getFitness().getTimeForTargetVehicle() + ";" + best.getFitness().getTimeForAllVehicles() + ";" + best.getDurations().stream().mapToInt(Integer::intValue).sum() + ";" + BruteForceService.getSignalIndex(best) + ";" + best.getDurations().toString() + ";" + best.getSignals().toString() + "\n";
        }catch (Exception e){
            e.printStackTrace();
        }
        StatisticsService.writeTxt(resultsFileName, str);
    }

    // writes information about the evolutionary process of a single EA/PSO to a predefined file
    private static void writeEvolutionAndDistance(){
        StatisticsService.deleteFile(evolutionFileName);
        StatisticsService.writeTxt(evolutionFileName, "Generation;MeanFitness\n");
        int i = 0;
        for(Double meanFitness: StatisticsService.getAverageFitnesses()){
            StatisticsService.writeTxt(evolutionFileName, i + ";" + meanFitness + "\n");
            i++;
        }

        StatisticsService.deleteFile(distancesFileName);
        StatisticsService.writeTxt(distancesFileName, "Generation;MeanDistance\n");
        i = 0;
        for(Double meanDistance: StatisticsService.getAverageDistance()){
            StatisticsService.writeTxt(distancesFileName, i + ";" + meanDistance + "\n");
            i++;
        }
    }



}

package common.data;

import DTO.Fitness;
import EA.data.Population;
import EA.operators.mutation.OffspringMutation;
import EA.operators.mutation.SwapMutation;
import common.service.*;
import configuration.Configuration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import transformers.ChromosomeTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A single solution / chromosome of the solution space
 * The representation is used for evolutionary algorithms and particle swarm optimisers
 */
@AllArgsConstructor // only for testing
public class Chromosome {

    private static final Logger logger = LogManager.getLogger(Chromosome.class.getName());

    /**
     *  The id is used when creating and reading the XML file of this chromosome
     *   -> The id has to be unique for one run of the application
     *   -> When initialising a new chromosome the next id is pulled from a threadsafe and previously filled list
     */
    @Getter
    private String id = ParallelisationService.uids.poll();

    /**
     * Part A of the chromosome representation, the signals per phase
     * Each signal in the list represents an index of the valid signals defined in the scenario configuration
     */
    @Getter
    @Setter
    private List<Integer> signals = new ArrayList<>();

    /**
     * Part B of the chromosome representation, the duration per phase
     * Each duration in the list represents the duration of the green time for the corresponding valid signal
     */
    @Getter
    @Setter
    private List<Integer> durations = new ArrayList<>();

    /**
     * The chromosomes fitness
     * The fitness is used to compare chromosomes and express their quality
     */
    private Fitness fitness;

    /**
     * An indicator of the species this chromosome belongs to
     * The species depends on the radius defined in the algorithm configuration
     */
    @Getter
    private int speciesClassifier;

    // An integer representing the generation the chromosome was born in
    @Getter
    @Setter
    private Integer generationBorn;

    /**
     * Main constructor to create new chromosome by the EA/PSO
     *  -> Creates a random chromosome with random signals and durations
     *  -> First the number of phases (length) is determined
     *  -> Then for each phase a random duration (within the allowed bounds is set)
     *  -> Additionally, a state is set that has a value between 0 and the number of available phases (excl.)
     *  -> A state can never be the same as the previous
     */
    public Chromosome(boolean initialise){
        if(!initialise) { return; }
        int length = ThreadLocalRandom.current().nextInt(Configuration.rc.minPhases(), Configuration.rc.maxPhases() + 1);
        int previousStateIndex = -1;
        for(int i = 0; i < length; i++){
            durations.add(ThreadLocalRandom.current().nextInt(Configuration.rc.minPhaseDuration(), Configuration.rc.maxPhaseDuration() + 1));
            int nextStateIndex = ThreadLocalRandom.current().nextInt(0, Configuration.sc.validSequences().size());
            nextStateIndex = previousStateIndex == nextStateIndex ? nextStateIndex + 1 >= Configuration.sc.validSequences().size() ? nextStateIndex - 1 : nextStateIndex + 1 : nextStateIndex;
            signals.add(nextStateIndex);
            previousStateIndex = nextStateIndex;
        }
        calculateSpeciesClassifier();
    }

    // Custom constructor primarily used by the brute force service
    public Chromosome(String id, List<Integer> signals, List<Integer> durations) {
        this.id = id;
        this.signals = signals;
        this.durations = durations;
        calculateSpeciesClassifier();
    }

    // Custom constructor primarily used by for unit tests
    public Chromosome(String id, List<Integer> signals, List<Integer> durations, Fitness fitness, Integer generationBorn) {
        this.id = id;
        this.signals = signals;
        this.durations = durations;
        this.fitness = fitness;
        this.generationBorn = generationBorn;
    }

    // Custom constructor to clone a chromosome
    public Chromosome(Chromosome chromosomeToClone){
        this.id = chromosomeToClone.id;
        this.signals = chromosomeToClone.getSignals();
        this.durations = chromosomeToClone.getDurations();
        this.fitness = chromosomeToClone.getFitness();
        this.generationBorn = chromosomeToClone.getGenerationBorn();
        this.speciesClassifier = chromosomeToClone.getSpeciesClassifier();
    }

    /**
     * Custom getter for fitness
     *  -> If the simulation fails the fitness of the chromosome is null.
     *  -> This should never happen, but in case it does happen, the application should still continue running.
     *  -> Therefore the worst possible fitness is returned in this event.
     * @return The fitness of the chromosome
     */
    public Fitness getFitness(){
        if (this.fitness == null) {
            logger.error("Fitness is null for chromosome " + id);
            return new Fitness(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        }
        return this.fitness;
    }

    /**
     * Calculation of the species of the individual
     * -> the species takes only the duration into account
     * -> based on the number of possible species in the configuration the species is calculated
     */
    private void calculateSpeciesClassifier(){
        int maxDurationSum = Configuration.rc.maxPhaseDuration() * Configuration.rc.maxPhases();
        int minDurationSum = Configuration.rc.minPhaseDuration() * Configuration.rc.minPhases();
        int numberOfSpecies = (int) Configuration.ac.diversityMechanismOptions().computeIfAbsent("numberOfSpecies", s -> 1);
        double stepSize = (double) (maxDurationSum - minDurationSum) / numberOfSpecies;
        int durationSum = durations.stream().mapToInt(Integer::intValue).sum();
        this.speciesClassifier = (int) Math.round(durationSum / stepSize);
    }

    /**
     * Evaluates (sets fitness of) this chromosome
     *  -> To evaluate a chromosome, the signals are exported as XML
     *  -> The final XML is then used for evaluating this chromosome
     *  -> The parameter population is only needed when using shared fitness
     */
    public void evaluate(Population population){
        String signalFileName = ChromosomeTransformer.transformWithName(this, id + "-signals.sig.xml");
        this.fitness = EvaluationService.evaluate(signalFileName, id);
        if(Configuration.ac.sharedFitness() && population != null){
            this.fitness.setSharedFitness(calculateSharedFitness(population));
        }
        StatisticsService.result.addEvaluatedChromosome(this);
    }

    /**
     * Calculates the shared fitness of the chromosome
     * The shared fitness is calculated with respect to the euclidean distance
     * The configured radius is taken into account
     * @param population the population
     * @return the shared fitness
     */
    private double calculateSharedFitness(Population population){
        List<Chromosome> chromosomesInRange = new ArrayList<>();
        List<Double> distances = new ArrayList<>();
        for(Chromosome chromosome : population.getChromosomes()){
            if(!chromosome.equals(this)){
                double distance = ChromosomeComparisonService.calculateEuclideanDistance(chromosome, this);
                if(distance <= Configuration.ac.sharedFitnessRadius()){
                    chromosomesInRange.add(chromosome);
                    distances.add(distance);
                }
            }
        }
        double sharedFitnessFactor = 0;
        for(int i = 0; i < chromosomesInRange.size(); i++){
            sharedFitnessFactor = sharedFitnessFactor + (1 - (distances.get(i) / Configuration.ac.sharedFitnessRadius()));
        }
        double sharedFitness = this.fitness.getValueRegardlessOfSharedFitness() * (1 + sharedFitnessFactor);
        if(sharedFitness < fitness.getValueRegardlessOfSharedFitness()){
            logger.error("Shared fitness is less than normal fitness.");
        }
        return sharedFitness;
    }

    // Mutates the chromosome based in mutation probability
    public void mutate() {
        if(Math.random() <= Configuration.ac.mutationProbability()){ this.signals = RepairService.repairSignals(Configuration.ac.mutationOperatorStates().mutate(signals)); }
        if(Math.random() <= Configuration.ac.mutationProbability()){ this.durations = RepairService.repairDurations(Configuration.ac.mutationOperatorDurations().mutate(durations)); }
        calculateSpeciesClassifier();
    }

    // Forces the duration of chromosome to mutate through thr offspring operator
    public void forceMutationOfDurationByApplyingOffspring() {
        OffspringMutation swapMutation = new OffspringMutation();
        RepairService.repairDurations(swapMutation.mutate(durations));
        calculateSpeciesClassifier();
    }

}

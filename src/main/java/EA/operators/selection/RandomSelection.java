package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Randomly selects two different parents
 */
public class RandomSelection implements SelectionOperator {

    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        List<Chromosome> chromosomeList = new ArrayList<>();;
        Arrays.asList(chromosomes.toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
        Collections.shuffle(chromosomeList);
        int randomIndexA = ThreadLocalRandom.current().nextInt(0, chromosomeList.size());
        int randomIndexB = ThreadLocalRandom.current().nextInt(0, chromosomeList.size());
        randomIndexB = randomIndexB == randomIndexA ? randomIndexB + 1 >= chromosomeList.size() ? randomIndexB - 1 : randomIndexB + 1 : randomIndexB;
        return new Parents(chromosomeList.get(randomIndexA), chromosomeList.get(randomIndexB));
    }

}

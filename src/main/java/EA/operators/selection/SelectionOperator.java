package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;

import java.util.concurrent.ConcurrentLinkedQueue;

public interface SelectionOperator {

    Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes);

}

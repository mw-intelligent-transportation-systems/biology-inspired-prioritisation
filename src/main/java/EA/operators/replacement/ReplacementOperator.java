package EA.operators.replacement;

import common.data.Chromosome;

import java.util.concurrent.ConcurrentLinkedQueue;

public interface ReplacementOperator {

    ConcurrentLinkedQueue<Chromosome> replace(ConcurrentLinkedQueue<Chromosome> chromosomes, Chromosome child);

}

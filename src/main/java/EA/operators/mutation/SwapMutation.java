package EA.operators.mutation;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SwapMutation implements MutationOperator {

    /**
     * Mutates a list by swapping two random genes
     * @param list the list to mutate
     * @return the mutated signals
     */
    @Override
    public List<Integer> mutate(List<Integer> list) {
        int[] randoms = ThreadLocalRandom.current().ints(0, list.size()).distinct().limit(2).toArray();
        int posA = randoms[0];
        int posB = randoms[1];
        Collections.swap(list, posA, posB);
        return list;
    }
}

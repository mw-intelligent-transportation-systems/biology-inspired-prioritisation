package EA.operators.crossover;

import DTO.Parents;
import common.data.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Determine longer and shorter parent
 * Create random point in longer parent
 * Take everything from index 0 to that point from parent A
 * Append parent B -> therefore min length is always given, as shorter parent is added
 * Return combination
 */
public class OnePointCrossover implements CrossoverOperator {

    private static final Logger logger = LogManager.getLogger(OnePointCrossover.class.getName());

    @Override
    public List<Integer> crossoverDurations(Parents parents) {

        Integer lengthA = parents.getParentA().getDurations().size();
        Integer lengthB = parents.getParentB().getDurations().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        int randomIndex = ThreadLocalRandom.current().nextInt(0, longerParent.getDurations().size());

        List<Integer> partFromParentA = longerParent.getDurations().subList(0, randomIndex);
        List<Integer> partFromParentB = shorterParent.getDurations();
        List<Integer> combination = new ArrayList<>(partFromParentA);
        combination.addAll(partFromParentB);

        return combination;
    }

    @Override
    public List<Integer> crossoverSignals(Parents parents) {
        Integer lengthA = parents.getParentA().getSignals().size();
        Integer lengthB = parents.getParentB().getSignals().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        int randomIndex = ThreadLocalRandom.current().nextInt(0, longerParent.getSignals().size());

        List<Integer> partFromParentA = longerParent.getSignals().subList(0, randomIndex + 1);
        List<Integer> partFromParentB = shorterParent.getSignals();
        List<Integer> combination = new ArrayList<>(partFromParentA);
        combination.addAll(partFromParentB);

        return combination;
    }

}

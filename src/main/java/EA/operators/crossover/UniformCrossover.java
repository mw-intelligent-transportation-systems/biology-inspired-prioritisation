package EA.operators.crossover;

import DTO.Parents;
import common.data.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Determine longer and shorter parent
 * For every bit in the longer parent decide which to take
 * When shorter parent ended, and parent A should not be taken abort.
 * Return combination
 */
public class UniformCrossover implements CrossoverOperator {

    private static final Logger logger = LogManager.getLogger(UniformCrossover.class.getName());

    @Override
    public List<Integer> crossoverDurations(Parents parents) {

        Integer lengthA = parents.getParentA().getDurations().size();
        Integer lengthB = parents.getParentB().getDurations().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        List<Integer> newDuration = new ArrayList<>();
        for(int i = 0; i < longerParent.getDurations().size(); i++){
            if(Math.random() < 0.5){
                newDuration.add(longerParent.getDurations().get(i));
            }else if (i < shorterParent.getDurations().size()){
                newDuration.add(shorterParent.getDurations().get(i));
            }else{
                i = longerParent.getDurations().size();
            }
        }

        return newDuration;
    }

    @Override
    public List<Integer> crossoverSignals(Parents parents) {
        Integer lengthA = parents.getParentA().getSignals().size();
        Integer lengthB = parents.getParentB().getSignals().size();
        Chromosome longerParent = lengthB > lengthA ? parents.getParentB() : parents.getParentA();
        Chromosome shorterParent = lengthB > lengthA ? parents.getParentA() : parents.getParentB();

        List<Integer> newSignals = new ArrayList<>();
        for(int i = 0; i < longerParent.getSignals().size(); i++){
            if(Math.random() < 0.5){
                newSignals.add(longerParent.getSignals().get(i));
            }else if (i < shorterParent.getSignals().size()){
                newSignals.add(shorterParent.getSignals().get(i));
            }else{
                i = longerParent.getSignals().size();
            }
        }

        return newSignals;
    }

}

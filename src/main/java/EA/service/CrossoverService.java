package EA.service;

import DTO.Parents;
import common.data.Chromosome;
import common.service.RepairService;
import configuration.Configuration;

import java.util.List;

/**
 * Service to crossover rwo chromosomes
 * Additionally the durations and signals are limited to be of the same length
 */
public class CrossoverService {

    public static Chromosome crossover(Parents parents){
        List<Integer> crossoverDuration = Configuration.ac.crossoverOperatorDuration().crossoverDurations(parents);
        crossoverDuration = RepairService.repairDurations(crossoverDuration);

        List<Integer> crossoverSignals = Configuration.ac.crossoverOperatorDuration().crossoverSignals(parents);
        crossoverSignals = RepairService.repairSignals(crossoverSignals);

        Chromosome child = new Chromosome(false);
        child.setDurations(crossoverDuration);
        child.setSignals(crossoverSignals);
        child = makeDurationsAndSignalsOfSameLength(child);
        return child;
    }

    public static Chromosome makeDurationsAndSignalsOfSameLength(Chromosome chromosome){
        Integer durations = chromosome.getDurations().size();
        Integer signals = chromosome.getSignals().size();

        if(durations > signals){
            List<Integer> newDurations = chromosome.getDurations().subList(0, chromosome.getSignals().size());
            chromosome.setDurations(newDurations);
        }else if(signals > durations){
            List<Integer> newSignals = chromosome.getSignals().subList(0, chromosome.getDurations().size());
            chromosome.setSignals(newSignals);
        }

        return chromosome;
    }

}

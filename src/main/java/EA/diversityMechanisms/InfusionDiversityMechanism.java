package EA.diversityMechanisms;

import EA.operators.replacement.WorstChildWithProbabilityReplacement;
import common.data.Chromosome;
import EA.data.Population;
import EA.operators.replacement.OldestChildReplacement;
import common.service.ParallelisationService;
import common.service.StatisticsService;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

public class InfusionDiversityMechanism extends AbstractDiversityMechanism implements DiversityMechanism {

    private static final Logger logger = LogManager.getLogger(InfusionDiversityMechanism.class.getName());

    @Override
    public void generateIds() {
        ParallelisationService.generateIds(Configuration.ac.populationSize() + ((Configuration.ac.numberOfChildren() + (int) Configuration.ac.diversityMechanismOptions().get("infusionNumberOfChildren")) * Configuration.ac.generations()));
    }

    /**
     * The evolution over one generation is split into
     *  -> The creation of children (parallelised)
     *  -> The potential replacement of chromosomes in the population by new children
     */
    public void evolvePopulation(int generation, Population population){
        ConcurrentLinkedQueue<Chromosome> children = new ConcurrentLinkedQueue<>();
        defaultMating(children, population, generation);
        defaultReplacement(children, population);
        int infusedChildren = infusePopulation(population, generation, (int) Configuration.ac.diversityMechanismOptions().get("infusionNumberOfChildren"));
        logger.info("Infused Population with " + infusedChildren + " children.");
        StatisticsService.result.setPopulationValues(population);
    }

    // The population is infused with a number of random chromosomes
    static int infusePopulation(Population population, int generation, int n){
        WorstChildWithProbabilityReplacement worstChildWithProbabilityReplacement = new WorstChildWithProbabilityReplacement();
        int infusedChildren;
        for(infusedChildren = 0; infusedChildren < n; infusedChildren++){
            Chromosome randomChromosome = new Chromosome(true);
            randomChromosome.setGenerationBorn(generation);
            randomChromosome.evaluate(population);
            worstChildWithProbabilityReplacement.replaceWithProb(population.getChromosomes(), randomChromosome, 0.0);
            if(!population.getChromosomes().contains(randomChromosome)){
                logger.error("Infusion failed. New chromosome not present in population.");
            }
        }
        return infusedChildren;
    }
}

package configuration.run.problemScenarios.revised;

import configuration.run.RunConfiguration;

public class ProblemScenario_III_I_Run implements RunConfiguration {

    @Override
    public Integer minPhases() {
        return 2;
    }

    @Override
    public Integer maxPhases() {
        return 4;
    }

    @Override
    public Integer minPhaseDuration() {
        return 5;
    }

    @Override
    public Integer maxPhaseDuration() {
        return 30;
    }

    @Override
    public Double importanceOfTargetVehicle() {
        return 0.7;
    }

    @Override
    public long timeLimit() {
        return 10000;
    }

    @Override
    public long timeLimitLocalSearch() {
        return 0;
    }
}

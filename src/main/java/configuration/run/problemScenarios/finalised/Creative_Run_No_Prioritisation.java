package configuration.run.problemScenarios.finalised;

import configuration.run.RunConfiguration;

public class Creative_Run_No_Prioritisation implements RunConfiguration {

    @Override
    public Integer minPhases() {
        return 4;
    }

    @Override
    public Integer maxPhases() {
        return 8;
    }

    @Override
    public Integer minPhaseDuration() {
        return 5;
    }

    @Override
    public Integer maxPhaseDuration() {
        return 60;
    }

    @Override
    public Double importanceOfTargetVehicle() {
        return 0.0;
    }

    @Override
    public long timeLimit() {
        return 20000;
    }

    @Override
    public long timeLimitLocalSearch() {
        return 0;
    }
}

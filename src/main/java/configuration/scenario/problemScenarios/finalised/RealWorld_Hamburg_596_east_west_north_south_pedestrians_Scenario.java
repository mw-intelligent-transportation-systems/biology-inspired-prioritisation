package configuration.scenario.problemScenarios.finalised;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(RealWorld_Hamburg_596_east_west_north_south_pedestrians_Scenario.class.getName());

    @Override
    public String network() {
        return "596-finalised/network-pedestrian.net.xml";
    }

    @Override
    public String routes() {
        return "596-finalised/e-w-n-s-pedestrian.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "596-finalised/signals-default-pedestrian.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrrrrrr");

        states.put("EAST_WEST_HEAD_START_GREEN", "rrrrrrrrrrrrrrGrGr");
        states.put("EAST_WEST_YELLOW_GREEN", "rrruuuurrruuuuGrGr");
        states.put("EAST_WEST_GREEN", "rrrgGGgrrrgGGgGrGr");
        states.put("EAST_WEST_EARLY_RED", "rrrgGGgrrrgGGgrrrr");
        states.put("EAST_WEST_YELLOW_RED", "rrryyyyrrryyyyrrrr");

        states.put("NORTH_SOUTH_HEAD_START_GREEN", "rrrrrrrrrrrrrrrGrG");
        states.put("NORTH_SOUTH_YELLOW_GREEN", "uuurrrruuurrrrrGrG");
        states.put("NORTH_SOUTH_GREEN", "gGgrrrrgGgrrrrrGrG");
        states.put("NORTH_SOUTH_EARLY_RED", "gGgrrrrgGgrrrrrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyrrrryyyrrrrrrrr");

        states.put("EAST_YELLOW_GREEN", "rrruuuurrrrrrrrrrr");
        states.put("EAST_GREEN", "rrrGGGGrrrrrrrrrrr");
        states.put("EAST_YELLOW_RED", "rrryyyyrrrrrrrrrrr");

        states.put("WEST_YELLOW_GREEN", "rrrrrrrrrruuuurrrr");
        states.put("WEST_GREEN", "rrrrrrrrrrGGGGrrrr");
        states.put("WEST_YELLOW_RED", "rrrrrrrrrryyyyrrrr");

        states.put("NORTH_YELLOW_GREEN", "uuurrrrrrrrrrrrrrr");
        states.put("NORTH_GREEN", "GGGrrrrrrrrrrrrrrr");
        states.put("NORTH_YELLOW_RED", "yyyrrrrrrrrrrrrrrr");

        states.put("SOUTH_YELLOW_GREEN", "rrrrrrruuurrrrrrrr");
        states.put("SOUTH_GREEN", "rrrrrrrGGGrrrrrrrr");
        states.put("SOUTH_YELLOW_RED", "rrrrrrryyyrrrrrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceEW = Arrays.asList("EAST_WEST_HEAD_START_GREEN", "EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_EARLY_RED", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceNS = Arrays.asList("NORTH_SOUTH_HEAD_START_GREEN", "NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_EARLY_RED", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceE = Arrays.asList("EAST_YELLOW_GREEN", "EAST_GREEN", "EAST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceW = Arrays.asList("WEST_YELLOW_GREEN", "WEST_GREEN", "WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceN = Arrays.asList("NORTH_YELLOW_GREEN", "NORTH_GREEN", "NORTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceS = Arrays.asList("SOUTH_YELLOW_GREEN", "SOUTH_GREEN", "SOUTH_YELLOW_RED", "ALL_RED");

        List<List<String>> sequences = Arrays.asList(validSequenceEW, validSequenceNS, validSequenceE, validSequenceW, validSequenceN, validSequenceS);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            // Init
            case 0:
                switch (targetState){
                    case 1:
                    case 2:
                        return Arrays.asList(1, 1, -1, 5, 3, 3);
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 5);
                    case 5:
                    case 6:
                        return Arrays.asList(1, -1, 3, 3);
                }
            case 1:
                switch (targetState){
                    case 2:
                        return Arrays.asList(1, 1, -1, 5, 3, 5);
                    default:
                        return Arrays.asList(1, -1, 3, 5);
                }
            case 2:
                switch (targetState){
                    case 1:
                        return Arrays.asList(1, 1, -1, 5, 3, 3);
                    default:
                        return Arrays.asList(1, -1, 3, 3);
                }
            case 3:
            case 4:
            case 5:
            case 6:
                switch (targetState){
                    case 1:
                        return Arrays.asList(1, 1, -1, 5, 3, 3);
                    case 2:
                        return Arrays.asList(1, 1, -1, 5, 3, 5);
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 5);
                    case 5:
                    case 6:
                        return Arrays.asList(1, -1, 3, 3);
                }
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 2500;
    }
}

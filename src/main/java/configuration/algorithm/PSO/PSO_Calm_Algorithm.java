package configuration.algorithm.PSO;

import EA.diversityMechanisms.DiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.mutation.MutationOperator;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.selection.SelectionOperator;
import PSO.Particle;
import configuration.algorithm.PSOAlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public class PSO_Calm_Algorithm implements PSOAlgorithmConfiguration {

    @Override
    public Boolean multiThreading() {
        return true;
    }

    @Override
    public Integer populationSize() {
        return 30;
    }

    @Override
    public boolean executeMultipleEvaluations() {
        return false;
    }

    @Override
    public Integer numberOfChildren() {
        return null;
    }

    @Override
    public Integer generations() {
        return 100;
    }

    @Override
    public boolean sharedFitness() {
        return false;
    }

    @Override
    public double sharedFitnessRadius() {
        return 50;
    }

    @Override
    public DiversityMechanism diversityMechanism() {
        return null;
    }

    @Override
    public Map<String, Object> diversityMechanismOptions() {
        return new HashMap<>();
    }

    @Override
    public SelectionOperator selectionOperator() {
        return null;
    }

    @Override
    public Map<String, Object> selectionOptions() {
        return new HashMap<>();
    }

    @Override
    public CrossoverOperator crossoverOperatorDuration() {
        return null;
    }

    @Override
    public CrossoverOperator crossoverOperatorSignals() {
        return null;
    }

    @Override
    public Double mutationProbability() {
        return null;
    }

    @Override
    public MutationOperator mutationOperatorStates() {
        return null;
    }

    @Override
    public MutationOperator mutationOperatorDurations() {
        return null;
    }

    @Override
    public ReplacementOperator replacementOperator() {
        return null;
    }

    @Override
    public Particle.PsoConfig psoConfig() {
        return Particle.PsoConfig.CALM;
    }
}

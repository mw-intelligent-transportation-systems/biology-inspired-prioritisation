package configuration.algorithm.problemScenarios.revised;

import EA.diversityMechanisms.DefaultDiversityMechanism;
import EA.diversityMechanisms.DiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.crossover.OnePointCrossover;
import EA.operators.mutation.MutationOperator;
import EA.operators.mutation.SwapMutation;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.replacement.WorstChildWithProbabilityReplacement;
import EA.operators.selection.SelectionOperator;
import EA.operators.selection.TournamentSelection;
import configuration.algorithm.AlgorithmConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class ProblemScenario_IV_Algorithm implements AlgorithmConfiguration {

    @Override
    public Boolean multiThreading() {
        return true;
    }

    @Override
    public Integer populationSize() {
        return 50;
    }

    @Override
    public boolean executeMultipleEvaluations() {
        return false;
    }

    @Override
    public boolean sharedFitness() {
        return false;
    }

    @Override
    public double sharedFitnessRadius() {
        return 0;
    }

    @Override
    public DiversityMechanism diversityMechanism() {
        return new DefaultDiversityMechanism();
    }

    @Override
    public Map<String, Object> diversityMechanismOptions() {
        Map options = new HashMap();
        return options;
    }

    @Override
    public Map<String, Object> selectionOptions() {
        Map options = new HashMap();
        options.put("tournamentSize", 5);
        return options;
    }

    @Override
    public Integer numberOfChildren() {
        return 10;
    }

    @Override
    public Integer generations() {
        return 100;
    }

    @Override
    public SelectionOperator selectionOperator() {
        return new TournamentSelection();
    }

    @Override
    public CrossoverOperator crossoverOperatorDuration() {
        return new OnePointCrossover();
    }

    @Override
    public CrossoverOperator crossoverOperatorSignals() {
        return new OnePointCrossover();
    }

    @Override
    public Double mutationProbability() {
        return 0.5;
    }

    @Override
    public MutationOperator mutationOperatorStates() {
        return new SwapMutation();
    }

    @Override
    public MutationOperator mutationOperatorDurations() {
        return new SwapMutation();
    }

    @Override
    public ReplacementOperator replacementOperator() {
        return new WorstChildWithProbabilityReplacement();
    }
}

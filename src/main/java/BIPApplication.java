import common.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main class to start application
 * See readme for usage
 */
public class BIPApplication {

    private static final Logger logger = LogManager.getLogger(BIPApplication.class.getName());

    /**
     * Starts the application with configuration name (see {@link configuration Configuration}) passed as optional first argument
     * @param args application arguments
     */
    public static void main(String[] args) {
        long start = System.nanoTime();
        // Preparation by setting config
        ExecutionService.setConfiguration(args.length < 1 ? "DEFAULT" : args[0]);

        // Read command line options
        boolean bruteForce = args.length >= 2 && args[1].equals("--bruteForce");
        boolean batchExecution = args.length >= 2 && args[1].equals("--batchRun");
        boolean randomExecution = args.length >= 2 && args[1].equals("--randomExecution");
        boolean robustnessEvaluation = args.length >= 2 && args[1].equals("--robustnessEvaluation");
        boolean robustnessEvaluationDefault = args.length >= 2 && args[1].equals("--robustnessEvaluationDefault");
        boolean switchConfig = args.length >= 2 && args[1].equals("--switchConfig");
        boolean pso = args.length >= 2 && args[1].equals("--PSO");

        try {

            // java -jar bip-1.0-SNAPSHOT.jar [config] --bruteForce [batchSize] [numberOfRandoms]
            if (bruteForce) {
                int batchSize = args.length >= 3 ? (new Integer(args[2])) : 50;
                int numberOfRandoms = args.length >= 4 ? (new Integer(args[3])) : -1;
                BruteForceService.prepareAndExecute(batchSize, numberOfRandoms);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --batchRun [numberOfRuns] [additionalConfig]
            } else if (batchExecution) {
                int numberOfRuns = args.length >= 3 ? (new Integer(args[2])) : 50;
                BatchRunService.prepareAndExecuteEA(numberOfRuns, args[0], args.length < 4 ? null : args[3]);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --randomExecution
            } else if (randomExecution){
                RandomRunService.prepareAndExecuteRandom(args[0]);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --robustnessEvaluation [numberOfEvaluations]
            } else if (robustnessEvaluation) {
                int numberOfEvaluations = args.length >= 3 ? (new Integer(args[2])) : 0;
                RobustnessEvaluationService.prepareAndExecuteEA(numberOfEvaluations, args[0], args.length < 4 ? null : args[3]);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --robustnessEvaluationDefault [numberOfEvaluations]
            }else if(robustnessEvaluationDefault){
                int numberOfEvaluations = args.length >= 3 ? (new Integer(args[2])) : 0;
                RobustnessEvaluationService.prepareAndExecuteDefault(numberOfEvaluations, args[0]);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --switchConfig [additionalConfig]
            }else if(switchConfig){
                ExecutionService.runEA(start, args.length < 3 ? "DEFAULT" : args[2]);

            // java -jar bip-1.0-SNAPSHOT.jar [config] --PSO [numberOfRuns]
            }else if(pso){
                if(args.length >= 3){
                    BatchRunService.prepareAndExecutePSO(new Integer(args[2]));
                }else{
                    ExecutionService.runPSO(start);
                }

            // java -jar bip-1.0-SNAPSHOT.jar [config]
            }else{
                ExecutionService.runEA(start, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);
    }



}

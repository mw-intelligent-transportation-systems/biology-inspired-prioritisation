package DTO;

import common.AbstractPopulation;
import common.data.Chromosome;
import EA.data.Population;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for results of running an EA/PSO
 */
@NoArgsConstructor
@Getter
public class Result {

    private Fitness fitnessOfDefaultSignals;
    private Chromosome bestOverallInInitialPopulation;
    private Chromosome bestForTargetVehicleInInitialPopulation;
    private Chromosome bestForAllVehiclesInInitialPopulation;
    private Chromosome bestOverall;
    private Chromosome bestForTargetVehicle;
    private Chromosome bestForAllVehicles;

    @Getter
    private AbstractPopulation finalPopulation;

    @Getter
    private List<Chromosome> evaluatedChromosomeList = new ArrayList<>();

    @Getter
    private int evaluatedChromosomes = 0;

    public Result(AbstractPopulation population){
        this.bestOverall = population.getBest();
        this.bestForTargetVehicle = population.getBestForTargetVehicle();
        this.bestForAllVehicles = population.getBestForAllVehicles();
    }

    public void setPopulationValues(AbstractPopulation population){
        this.bestOverall = population.getBest();
        this.bestForTargetVehicle = population.getBestForTargetVehicle();
        this.bestForAllVehicles = population.getBestForAllVehicles();
        this.finalPopulation = population;
    }

    public void setInitialPopulationValues(AbstractPopulation initialPopulationValues){
        this.bestOverallInInitialPopulation = initialPopulationValues.getBest();
        this.bestForTargetVehicleInInitialPopulation = initialPopulationValues.getBestForTargetVehicle();
        this.bestForAllVehiclesInInitialPopulation = initialPopulationValues.getBestForAllVehicles();
    }

    public void setFitnessOfDefaultSignals(Fitness fitnessOfDefaultSignals) {
        this.fitnessOfDefaultSignals = fitnessOfDefaultSignals;
    }

    public void addEvaluated(){
        evaluatedChromosomes++;
    }

    public void addEvaluatedChromosome(Chromosome chromosome){
        evaluatedChromosomeList.add(chromosome);
    }

}

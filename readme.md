# BIP - **B**iology **I**nspired **P**rioritisation

## Motivation
Urban mobility presents one of the most challenging problems of the 21st century. The steady rise in population and urbanisation results in more people seeking to move through relatively less space. In combination with the increasing awareness of climate change, the need for efficient public transportation is substantial. To improve urban efficiency and prioritise public transportation, traffic engineers’ resort to improving traffic signals. Hence, a need for smarter systems that utilise the recent advancements in sensors for capturing real time traffic data and computing is present. This motivated a large number of research in traffic signal control and traffic signal prioritisation. Prior research demonstrated the suitability of evolutionary algorithms for traffic signal control and traffic signal prioritisation. However, existing real time approaches often neglect real-world traffic constraints or put little focus on realistic traffic simulation.

This research proposes a novel approach to find improved signal plans by an evolutionary algorithm in real time. A novel representation allows for a creative evolutionary process while ensuring traffic safety. Candidate solutions are efficiently evaluated by a well-established microscopic traffic simulator (SUMO) in real time. The algorithm is applied to various traffic scenarios on a real-world intersection in Hamburg, Germany. Results showed that the system is able to find near-optimal signal plans in under 20 seconds. In all scenarios, the results of current signal plans could be enhanced, more significant improvements could be achieved as traffic saturation increased.

## About
BIP is written in java and provides a command line interface (see usage). With BIP an evolutionary algorithm or particle swarm optimiser can be executed using a given configuration. Every candidate solution is evaluated by the well-established microscopic traffic simulator [SUMO](https://sumo.dlr.de). A configuration consists of an algorithm-, a scenario- and a run-sub-configuration.
* The algorithm-sub-configuration defines the parameters/operators for the evolutionary algorithm (e.g. population size).
* The scenario-sub-configuration defines the location of required files for the traffic simulator as well as valid signals, intergreen times and the maximum time a simulation should run before forcefully aborting.
* The run-sub-configuration constraints the solution space by limiting the number of phases as well as the duration for any phase. Additionally, a time limit for the algorithm as well as the importance of the bus is defined.

Usually, the system runs on multiple threads for efficient simulation over a 20 second time limit. The system takes as many resources as possible. If you run BIP locally, your system might become unresponsive during execution.

## Usage
BIP supports various modes which are briefly outlined here. Where the individual parameters vary, each mode needs at least one configuration. Configurations can be defined in the configuration class. A list of some predefined configurations is shown in the next section.

All commands assume that you are in the **build/libs** directory. First, the general syntax is given, then an example is provided.

### Run EA once using a given configuration
```shell
java -jar bip-1.0-SNAPSHOT.jar [config]
java -jar bip-1.0-SNAPSHOT.jar C-EW-FC
```

### Run EA once using two given configurations
The algorithm switches between the first and second configuration ever 10 generations.
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --switchConfig [additionalConfig]
java -jar bip-1.0-SNAPSHOT.jar C-EW-EX --switchConfig C-EW-FC
```

### Run EA n times using a given configuration
Statistics about the runs are written to various seperate csv files.
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --batchRun [numberOfRuns]
java -jar bip-1.0-SNAPSHOT.jar C-EW-FC --batchRun 2
```

### Run EA n times using two given configurations
Statistics about the runs are written to various seperate csv files. The algorithm switches between the first and second configuration ever 10 generations.
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --batchRun [numberOfRuns] [additionalConfig]
java -jar bip-1.0-SNAPSHOT.jar C-EW-FC --batchRun 2 C-EW-EX
```

### Run PSO once using a given configuration
The config used has to be a PSO config.
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --PSO
java -jar bip-1.0-SNAPSHOT.jar C-EW-PSOC --PSO 
```

### Run PSO n times using a given configuration
The config used has to be a PSO config. Statistics about the runs are written to various seperate csv files.
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --PSO [numberOfRuns]
java -jar bip-1.0-SNAPSHOT.jar C-EW-PSOC --PSO 2
```

### Randomly generate and evaluate solutions given a configuration
```shell
java -jar bip-1.0-SNAPSHOT.jar [config] --randomExecution
java -jar bip-1.0-SNAPSHOT.jar C-EW-FC --randomExecution
```

## Configurations
These configurations are defined in the configuration class. There, additional configuration can also be defined.

Each configuration string consists of C-[TRAFFIC]-[SYSTEM]

#### The available traffic configurations are:
| Traffic Configuration | Description                                                                                                                                                   |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| BASE|Low traffic saturation on a four arm intersection, bus moving east to west.                                                                                                                                                                          |
| EW|Increased traffic from east and west, bus moving from east to west                                                                                                                                                                                     |
| NS|Increased traffic from north and south, bus moving from south to west                                                                                                                                                                                  |
| EWS|Increased traffic from east, west and south, bus moving from east to west                                                                                                                                                                             |
| EWNS|Increased traffic from east, west, north and south, bus moving from south to west                                                                                                                                                                    |
| BASE-PED|Low traffic saturation (including pedestrians) on a four arm intersection, bus moving east to west.                                                                                                                                              |
| EWNS-PED|Increased traffic (including pedestrians) from east, west, north and south, bus moving from south to west                                                                                                                                        |


#### The available system configurations are:
| System Configuration | Description                                                                                                                                                   |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| FC|EA convergences quickly over 20 seconds                                                                                                                                                                                                                  |
| EX|EA convergences slower over 20 seconds, as mating is restricted by distance                                                                                                                                                                            |
| FC-LS|EA convergences quickly  over 15 seconds and uses the last 5 seconds to perform a local search on the five best solutions                                                                                                                             |
| EX-LS|EA convergences slower, as mating is restricted by distance, over 15 seconds and uses the last 5 seconds to perform a local search on the five best solutions                                                                                       |
| PSOC|PSO config that runs over 20 seconds and acts calmly (medium importance of neighbourhood best and medium velocity caps)                                                                                                                                |
| PSOA|PSO config that runs over 20 seconds and acts actively (high importance of neighbourhood best and high velocity caps)                                                                                                                                  |
| FC-ME|EA convergences quickly over 20 seconds and evaluates each candidate solution three times under slightly different traffic conditions to tests the solutions robustness. Works only on BASE-PED and EWNS-PED                                          |
| EX-ME|EA convergences slower over 20 seconds , as mating is restricted by distance and evaluates each candidate solution three times under slightly different traffic conditions to tests the solutions robustness. Works only on BASE-PED and EWNS-PED   |
| FC-ME-RT|EA convergences quickly over 10 seconds and evaluates each candidate solution three times under slightly different traffic conditions to tests the solutions robustness. Works only on BASE-PED and EWNS-PED                                       |
| EX-ME-RT|EA convergences slower over 10 seconds, as mating is restricted by distance and evaluates each candidate solution three times under slightly different traffic conditions to tests the solutions robustness. Works only on BASE-PED and EWNS-PED |


Any combination is possible (e.g. C-BASE-FC, C-NS-EX, C-EWNS-PSOA).

## Configuration for ITS World Congress Hamburg 2021*
In the paper submitted for ITS World Congress Hamburg 2021 one experiment is presented.

The experiment uses the configuration *C-BASE-PED-FC-ME* for the basic and *C-EWNS-PED-FC-ME* for the oversaturated scenario.  

To start the experiment use 
```shell
java -jar bip-1.0-SNAPSHOT.jar C-BASE-PED-FC-ME
java -jar bip-1.0-SNAPSHOT.jar C-EWNS-PED-FC-ME
```

## Get In Touch
If you would like to get in touch, you can find me on [twitter](https://twitter.com/milanwittpohl) or send me an email at [hello@milanwittpohl.com](mailto:hello@milanwittpohl.com).
